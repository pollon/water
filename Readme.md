# Water
_Effortless os without excess_

> Wu wei, "non-doing" is daoist for natural action: acting without struggle or excess effort. Flow. 
> Wu wei is often symbolized with water. Think of a river flowing naturally around everything it meets.

## Water
Water is a minimal os environment[^1] designed around a *way* of using your computer.

Installing Water should be fast and frictionless. You should never be afraid to delete, and re-install your OS.

## Why water?
Water is the byproduct of countless conversations revolving around a simple question: why, when I turn on my computer, do I lose my ideas? 

You have a moment of clarity. A question, an answer. You decide to do work on it. You want to explore the idea. Push it around. Make it real. 

Then you open your laptop.

The screen lights up with thousands of pixels begging for your attention:

Apps. Colorful icons. Notifications chimes and jiggles, greetings and warnings. All competing for your attention. Click here, tap there, do this, no that... 
Oh goody you're here! We have so much for you to think about, worry about, update, adjust...

**Indecision**. The death of all ideas.

Indecision leads to distraction. A stream of subconcious habits: Blogs, news, tech news, apps, hardware, doodling, settings, app-shopping, more settings...

Wait, what were you thinking about? 

Two hours later your interface is a screaming mess of windows stacked on windows and more browser tabs than fit on your 15" high-res display. Dissatisfied you settle for dreaming. Tomorrow.

**We didn't choose any of this**. Not up front. We call this **passive computing**, or, grazing. Guided aimlessly from distraction-to-distraction, by interface patterns and ui designs that are not in our best interest.

We're fighting a losing battle. Throw a young idea into that evironment and watch it get clicked, swiped, and approved away.

## This is Water

What if, in that moment of clarity and decision, you opened your laptop, turned on your screen, and saw:
**nothing**?

Blank. Empty.

Still have your idea? Great, type a command to access the correct tool and get to work.

Nothing happens until you tell your machine what to do. We call this **active computing**.

Water has the essentials of computing: Text editor, Web Browser, Terminal. Built on an essentials-only foundation.

## A didactic little parable
> There are these two young fish swimming along and they happen to meet an older fish swimming the other way, who nods at them and says "Morning, boys. How's the water?" The two fish swim on for a bit, and then eventually one of them looks at the other and goes: "What the hell is water?".

> Learning how to think really means learning how to exercise some control over how & what you think. It means being conscious & aware enough to choose what you pay attention to & to choose how you construct meaning from experience.

> ... The alternative is unconsciousness, the default setting, the rat race, the constant gnawing sense of having had, and lost, some infinite thing.

�David Foster Wallace, [watch the full talk on Water](https://youtu.be/8CrOL-ydFMI)

---
[^1] It is called an _environment_ because the actual OS Water is based on changes over time. We base Water on "lean", "minimal", "simple" operating systems (past OS's include: Arch Linux, Void Linux, Alpine Linux). Currently based on stock, OpenBSD.